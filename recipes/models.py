from enum import unique
from re import M
from unicodedata import name
from django.db import models
from django.forms import CharField


# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.name + " " + self.abbreviation


class FoodItem(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return "The food item " + self.name


class Ingredient(models.Model):
    name = models.CharField(max_length=30, null=True)
    amount = models.FloatField()
    recipe = models.ForeignKey(
        "Recipe",
        related_name="ingredients",
        on_delete=models.CASCADE,
        null=True,
    )
    measure = models.ForeignKey("Measure", on_delete=models.PROTECT, null=True)
    food = models.ForeignKey("FoodItem", on_delete=models.PROTECT, null=True)

    def __str__(self):
        return f"Ingredients are: {self.name}"


class Step(models.Model):
    recipe = models.ForeignKey(
        "Recipe", related_name="steps", on_delete=models.CASCADE
    )
    order = models.SmallIntegerField()
    directions = models.CharField(max_length=300)
    food_items = models.ManyToManyField("FoodItem", null=True, blank=True)

    def __str__(self):
        return f"Step {self.order}"
